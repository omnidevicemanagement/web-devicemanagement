package com.bca.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.web.model.Borrower;
import com.bca.web.serviceImpl.BorrowerServiceImpl;

@RestController
@RequestMapping("/borrower")
@CrossOrigin(origins="http://localhost:4200")
public class BorrowerController {
	
	@Autowired
	private BorrowerServiceImpl borrowerServiceImpl;
	
	@GetMapping
	public List<Borrower> listBorrowerWeb() {		
		return borrowerServiceImpl.getBorrowers();
	}
	
	@GetMapping("{id}")
	public Borrower findBorrowerWeb(@PathVariable int id) {
		return borrowerServiceImpl.getBorrowerById(id);
	}
	
	@PostMapping
	public Borrower addBorrowerWeb(@RequestBody Borrower borrower) {
		return borrowerServiceImpl.addBorrower(borrower);
	}

}
