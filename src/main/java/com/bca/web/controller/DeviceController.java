package com.bca.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.web.model.DetailDevice;
import com.bca.web.model.Device;
import com.bca.web.serviceImpl.DeviceServiceImpl;

@RestController
@RequestMapping("/device")
@CrossOrigin(origins="http://localhost:4200")
public class DeviceController {
	
	@Autowired
	private DeviceServiceImpl deviceServiceImpl;
	
	@GetMapping
	public List<Device> listDeviceWeb() {
		return deviceServiceImpl.getDevices();
	}
	
	@GetMapping("/available")
	public List<Device> listAvailableDeviceWeb() {
		return deviceServiceImpl.getAvailableDevices();
	}
	
	@GetMapping("/detail")
	public List<DetailDevice> detailDeviceWeb() {
		return deviceServiceImpl.getDeviceDetails();
	}
	
	@GetMapping("{id}")
	public Device findDeviceWeb(@PathVariable int id) {
		return deviceServiceImpl.getDeviceById(id);
	}
	
	@PostMapping
	public Device addDeviceWeb(@RequestBody Device device) {
		return deviceServiceImpl.addDevice(device);
	}
	
	@PutMapping
	public Device updateDeviceWeb(@RequestBody Device device) {
		return deviceServiceImpl.updateDevice(device);
	}
	
	@DeleteMapping("{id}")
	public void deleteDeviceWeb(@PathVariable int id) {
		deviceServiceImpl.deleteDevice(id);
	}
	
}
