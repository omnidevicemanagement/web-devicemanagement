package com.bca.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.web.model.Loan;
import com.bca.web.model.LoanUser;
import com.bca.web.serviceImpl.LoanServiceImpl;

@RestController
@RequestMapping("/loan")
@CrossOrigin(origins="http://localhost:4200")
public class LoanController {
	
	@Autowired
	private LoanServiceImpl loanServiceImpl;
	
	@PostMapping
	public Loan insertLoanWeb(@RequestBody Loan loan) {
		return loanServiceImpl.insertLoan(loan);
	}
	
	@GetMapping("/current/{idBorrower}")
	public List<LoanUser> currentLoanUserWeb(@PathVariable int idBorrower) {
		return loanServiceImpl.getCurrentLoanUser(idBorrower);
	}
	
	@GetMapping("/history/{idBorrower}")
	public List<LoanUser> historyLoanUserWeb(@PathVariable int idBorrower) {
		return loanServiceImpl.getHistoryLoanUser(idBorrower);
	}
	
	@GetMapping("/return/{id}")
	public Loan returnLoanWeb(@PathVariable int id) {
		return loanServiceImpl.returnLoan(id);
	}
	
	@GetMapping
	public List<Loan> getLoanWeb() {
		return loanServiceImpl.getLoans();
	}
}
