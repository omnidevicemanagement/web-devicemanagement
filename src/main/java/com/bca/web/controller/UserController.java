package com.bca.web.controller;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.web.model.WebUser;
import com.bca.web.repository.WebUserRepository;

@RestController
@RequestMapping("users")
public class UserController {
	private WebUserRepository webUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(WebUserRepository webUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.webUserRepository = webUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody WebUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        webUserRepository.save(user);
    }
}
