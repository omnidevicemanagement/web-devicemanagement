package com.bca.web.model;

import java.util.Date;

public class DetailDevice {

	private int idDevice;
	private String namaDevice;
	private String type;
	private String serialNumber;
	private String operatingSystem;
	private String deviceDescription;
	private String bookStatus;
	private String loanID;
	private Date fromLoan;
	private Date toLoan;
	private String idBorrower;
	private String namaBorrower;
	
	public int getIdDevice() {
		return idDevice;
	}
	public void setIdDevice(int idDevice) {
		this.idDevice = idDevice;
	}
	public String getNamaDevice() {
		return namaDevice;
	}
	public void setNamaDevice(String namaDevice) {
		this.namaDevice = namaDevice;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getDeviceDescription() {
		return deviceDescription;
	}
	public void setDeviceDescription(String deviceDescription) {
		this.deviceDescription = deviceDescription;
	}
	public String getBookStatus() {
		return bookStatus;
	}
	public void setBookStatus(String bookStatus) {
		this.bookStatus = bookStatus;
	}
	public String getLoanId() {
		return loanID;
	}
	public void setLoanID(String loanId) {
		this.loanID = loanId;
	}
	public Date getFromLoan() {
		return fromLoan;
	}
	public void setFromLoan(Date fromLoan) {
		this.fromLoan = fromLoan;
	}
	public Date getToLoan() {
		return toLoan;
	}
	public void setToLoan(Date toLoan) {
		this.toLoan = toLoan;
	}
	public String getIdBorrower() {
		return idBorrower;
	}
	public void setIdBorrower(String idBorrower) {
		this.idBorrower = idBorrower;
	}
	public String getNamaBorrower() {
		return namaBorrower;
	}
	public void setNamaBorrower(String namaBorrower) {
		this.namaBorrower = namaBorrower;
	}
	
	
}
