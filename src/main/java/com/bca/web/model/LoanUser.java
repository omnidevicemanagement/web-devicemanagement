package com.bca.web.model;

import java.util.Date;

public class LoanUser {

	private int loanID;
	private String nama;
	private Date fromLoan;
	private Date toLoan;
	
	public int getLoanID() {
		return loanID;
	}
	public void setLoanID(int loanId) {
		this.loanID = loanId;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public Date getFromLoan() {
		return fromLoan;
	}
	public void setFromLoan(Date fromLoan) {
		this.fromLoan = fromLoan;
	}
	public Date getToLoan() {
		return toLoan;
	}
	public void setToLoan(Date toLoan) {
		this.toLoan = toLoan;
	}
	
}
