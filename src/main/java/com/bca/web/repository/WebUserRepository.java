package com.bca.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bca.web.model.WebUser;

public interface WebUserRepository extends JpaRepository<WebUser, Long> {
	WebUser findByUsername(String username);
}
