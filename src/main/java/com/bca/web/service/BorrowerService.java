package com.bca.web.service;

import java.util.List;

import com.bca.web.model.Borrower;

public interface BorrowerService {

	public List<Borrower> getBorrowers();
	public Borrower getBorrowerById(int id);
	public Borrower addBorrower(Borrower borrower);
	
}
