package com.bca.web.service;

import java.util.List;

import com.bca.web.model.DetailDevice;
import com.bca.web.model.Device;

public interface DeviceService {

	public List<Device> getDevices();
	public List<Device> getAvailableDevices();
	public List<DetailDevice> getDeviceDetails();
	public Device getDeviceById(int id);
	public Device addDevice(Device device);
	public Device updateDevice(Device device);
	public void deleteDevice(int id);
}
