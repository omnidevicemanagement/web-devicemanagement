package com.bca.web.service;

import java.util.List;

import com.bca.web.model.Loan;
import com.bca.web.model.LoanUser;

public interface LoanService {

	public List<Loan> getLoans();
	public List<LoanUser> getCurrentLoanUser(int idBorrower);
	public List<LoanUser> getHistoryLoanUser(int idBorrower);
	public Loan insertLoan(Loan loan);
	public Loan returnLoan(int id);
	
}
