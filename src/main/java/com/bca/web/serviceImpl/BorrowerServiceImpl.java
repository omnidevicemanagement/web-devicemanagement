package com.bca.web.serviceImpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bca.web.model.Borrower;
import com.bca.web.service.BorrowerService;

import static com.bca.web.model.NamingConstant.*;

@Service
public class BorrowerServiceImpl implements BorrowerService {

	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public List<Borrower> getBorrowers() {
		try {
			ResponseEntity<List<Borrower>> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "borrower", 
					HttpMethod.GET,
					null,
					new ParameterizedTypeReference<List<Borrower>>() {});
			List<Borrower> listOfBorrower = responseEntity.getBody();
			return listOfBorrower;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Borrower getBorrowerById(int id) {
		try {
			ResponseEntity<Borrower> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "borrower/" + id, 
					HttpMethod.GET, 
					null, 
					Borrower.class);
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}

	@Override
	public Borrower addBorrower(Borrower borrower) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Borrower> entity = new HttpEntity<Borrower>(borrower, headers);
		
		try {
			ResponseEntity<Borrower> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "borrower",
					HttpMethod.POST, 
					entity, 
					Borrower.class);
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
