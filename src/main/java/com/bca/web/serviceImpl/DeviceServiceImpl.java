package com.bca.web.serviceImpl;

import static com.bca.web.model.NamingConstant.BASE_URL;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bca.web.model.DetailDevice;
import com.bca.web.model.Device;
import com.bca.web.service.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService {
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public List<Device> getDevices() {
		try {
			ResponseEntity<List<Device>> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "device", 
					HttpMethod.GET, 
					null, 
					new ParameterizedTypeReference<List<Device>>(){});
			List<Device> listOfDevices = responseEntity.getBody();
			return listOfDevices;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Device> getAvailableDevices() {
		try {
			ResponseEntity<List<Device>> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "device/available", 
					HttpMethod.GET, 
					null, 
					new ParameterizedTypeReference<List<Device>>(){});
			List<Device> listOfAvailbleDevices = responseEntity.getBody();
			return listOfAvailbleDevices;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<DetailDevice> getDeviceDetails() {
		try {
			ResponseEntity<List<DetailDevice>> responseEntity = 
					restTemplate.exchange(
							BASE_URL + "device/detail", 
							HttpMethod.GET, 
							null, 
							new ParameterizedTypeReference<List<DetailDevice>>() {});
			
			List<DetailDevice> listOfDetails = responseEntity.getBody();
			return listOfDetails;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Device getDeviceById(int id) {
		try {
			ResponseEntity<Device> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "device/" + id,
					HttpMethod.GET,
					null,
					Device.class);
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Device addDevice(Device device) {
		try {
			Device response = 
				restTemplate.postForObject(
					BASE_URL + "device", 
					device, 
					Device.class);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Device updateDevice(Device device) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Device> entity = new HttpEntity<Device>(device, headers);

		try {
			ResponseEntity<Device> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "device", 
					HttpMethod.PUT, 
					entity, 
					Device.class);
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteDevice(int id) {
		try {
			restTemplate.delete(BASE_URL + "device/" + id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
