package com.bca.web.serviceImpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bca.web.model.Loan;
import com.bca.web.model.LoanUser;
import com.bca.web.service.LoanService;

import static com.bca.web.model.NamingConstant.*;

@Service
public class LoanServiceImpl implements LoanService {

	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public List<Loan> getLoans() {
		try {
			ResponseEntity<List<Loan>> responseEntity =
				restTemplate.exchange(
					BASE_URL + "loan", 
					HttpMethod.GET, 
					null, 
					new ParameterizedTypeReference<List<Loan>>() {});
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<LoanUser> getCurrentLoanUser(int idBorrower) {
		try {
			ResponseEntity<List<LoanUser>> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "loan/current/" + idBorrower, 
					HttpMethod.GET, 
					null, 
					new ParameterizedTypeReference<List<LoanUser>>() {});
			List<LoanUser> listOfCurrentLoan = responseEntity.getBody();
			return listOfCurrentLoan;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<LoanUser> getHistoryLoanUser(int idBorrower) {
		try {
			ResponseEntity<List<LoanUser>> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "loan/history/" + idBorrower, 
					HttpMethod.GET, 
					null, 
					new ParameterizedTypeReference<List<LoanUser>>() {});
			List<LoanUser> listOfHistoryLoan = responseEntity.getBody();
			return listOfHistoryLoan;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Loan insertLoan(Loan loan) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Loan> entity = new HttpEntity<Loan>(loan, headers);
		try {
			ResponseEntity<Loan> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "loan",
					HttpMethod.POST,
					entity,
					Loan.class);
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Loan returnLoan(int id) {
		try {
			ResponseEntity<Loan> responseEntity = 
				restTemplate.exchange(
					BASE_URL + "loan/return/" + id, 
					HttpMethod.GET, 
					null, 
					Loan.class);
			return responseEntity.getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
