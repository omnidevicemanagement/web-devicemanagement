package com.bca.web.serviceImpl;

import static java.util.Collections.emptyList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bca.web.model.WebUser;
import com.bca.web.repository.WebUserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	private WebUserRepository webUserRepository;

    public UserDetailsServiceImpl(WebUserRepository applicationUserRepository) {
        this.webUserRepository = applicationUserRepository;
    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		WebUser applicationUser = webUserRepository.findByUsername(username);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
	}

}
